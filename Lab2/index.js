var server_port = 65432;
var server_addr = "10.24.3.136";   // the IP address of your Raspberry PI

function client(){
    const net = require('net');
    var input = document.getElementById("myName").value;

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        client.write(`${input}\r\n`);
    });
    console.log(client)
    // get the data from the server
    client.on('data', (data) => {
        document.getElementById("greet_from_server").innerHTML = data;
        console.log(data.toString());
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });
}

function greeting(){
    console.log('click')
    // get the element from html
    var name = document.getElementById("myName").value;
    // update the content in html
    //document.getElementById("greet").innerHTML = "Hello " + name + " !";
    client();
}
