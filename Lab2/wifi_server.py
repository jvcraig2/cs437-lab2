import socket
import picar_4wd as fc

POW = 100
HOST = "10.24.3.136"  # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    fc.start_speed_thread()
    s.bind((HOST, PORT))
    s.listen()

    try:
        while 1:
            client, clientInfo = s.accept()
            # receive 1024 Bytes of message in binary format
            data = client.recv(1024)
            if data != b"":
                if data == b'update_req\r\n':
                    stats_str = f'{fc.get_distance_at(0)},{fc.utils.power_read()},{fc.utils.cpu_temperature()},{fc.speed_val()}'
                    client.sendall(stats_str.encode())
                elif data == b'up\r\n':
                    fc.forward(POW)
                elif data == b'left\r\n':
                    fc.turn_right(POW)
                elif data == b'down\r\n':
                    fc.backward(POW)
                elif data == b'right\r\n':
                    fc.turn_left(POW)
                elif data == b'space\r\n':
                    fc.stop()
    except Exception as e:
        print(e)
    finally:
        pass
