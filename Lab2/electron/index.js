document.onkeydown = updateKey;
document.onkeyup = resetKey;

var server_port = 65432;
var server_addr = "10.24.3.136";   // the IP address of your Raspberry PI

function client(input) {

    const net = require('net');

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        console.log('connected to server!');
        client.write(`${input}\r\n`);
    });

    // get the data from the server
    client.on('data', (data) => {
        if (input == 'update_req') {
            // distance, power, temp, speed

            split_datas = data.toString().split(',')

            document.getElementById("voltage").innerHTML = split_datas[1];
            document.getElementById("us").innerHTML = split_datas[0];
            document.getElementById("temp").innerHTML = split_datas[2];
            document.getElementById("speed").innerHTML = split_datas[3];
        }
        console.log(data.toString());
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });
}

// for detecting which key is been pressed w,a,s,d
function updateKey(e) {

    e = e || window.event;

    if (e.keyCode == '87') {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        client("up");
    }
    else if (e.keyCode == '83') {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        client("down");
    }
    else if (e.keyCode == '65') {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        client("left");
    }
    else if (e.keyCode == '68') {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        client("right");
    }
    else if (e.keyCode == '32') {
        client('space');
    }
}

// reset the key to the start state 
function resetKey(e) {

    e = e || window.event;

    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";
}


// update data for every 50ms
setInterval(function () {
    // get image from python server
    client('update_req');
}, 200);


